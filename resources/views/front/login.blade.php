@extends('front.layout')
@section('content')
    <main>
        <div class="login">
            <div class="login__left">
                <figure>
                    <img src="{{ asset('images/new_logo.png') }}" alt="">
                </figure>
            </div>
            <div class="login__right">
                <div class="login__cnt">
                    <h4 class="login__cnt--title">Ingrese su documento</h4>
                    <label for="" class="login__cnt--label">DNI o CE</label>
                    <input type="text" placeholder="Escriba" class="login__cnt--input" maxlength="8">
                    <div class="login__accept">
                        <div class="login__accept--input">
                            <input type="checkbox">
                        </div>
                        <label for="">
                            He leído y acepto la
                            <a href="">pol ítica de privacidad</a>
                        </label>
                    </div>
                    <a href="/" class="login__cnt--btn">Ingresar</a>
                </div>
            </div>
        </div>
    </main>

@endsection