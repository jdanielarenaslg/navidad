<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NAVIDAD</title>
    <link rel="icon" type="image/png" href="{{ asset('images/fav.png') }}">
{{--    <link rel="stylesheet" href="{{ asset('js/plugins/swiper/swiper.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>


@include('front.sections.header')
@yield('content')
@include('front.sections.footer')

<script src="{{ asset('js/jquery.js') }}"></script>
{{--<script src="{{ asset('js/plugins/swiper/swiper.js') }}"></script>--}}
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
