<header class="header">
    <div class="header__wrapper">
        <div class="header__logo">
            <figure class="header__image">
                <img src="{{ asset('images/logo_small.png') }}" alt="">
            </figure>
        </div>
        <div class="header__right">
            <p>Feliz Navidad y próspero 2022</p>
            <a href="" class="header__right--close">
                <span>Salir</span>
                <img src="{{ asset('images/log-in.png') }}" alt="">
            </a>
        </div>
    </div>
</header>